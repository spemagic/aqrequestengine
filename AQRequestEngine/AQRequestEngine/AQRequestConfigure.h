
// 根据不同项目调整的可变参数
#define BASIC_URL   "http://192.168.5.11/inf/"

static CGFloat const TIME_OUT = 30.0;
static CGFloat const INVALID_INDEX = -1;
static NSString *const CONVENSION_KEY = @"894D94361A243577F0A497C4EAB6462A178900022D1D95B2EAE04";

typedef enum { // HTTP请求方式
    AQRequestMethodGet = 0,
    AQRequestMethodPost
} AQRequestMethod;

typedef enum { // Request列表，每添加一个request，需要新增一个枚举项
    AQRequestTypeNone = 0,
    AQRequestTypeLogin,
    AQRequestTypeWeather,
    AQRequestTypeUploadHeadImage,
    AQRequestTypeQueryAllQuestionsAndAnswers,
    AQRequestTypeInvalid = 10000
} AQRequestType;

typedef struct {
    char *basicUrl; // 基础url，预留选项，防止不同request基础url不相同
    char *appendingUrl; // 附加url
    AQRequestType requestType; // Request类型
} AQRequestDictionary;

static AQRequestDictionary const reqeustDictionary[] = {
    { BASIC_URL, "login.htm", AQRequestTypeLogin },
    { BASIC_URL, "uploadFile.htm", AQRequestTypeUploadHeadImage },
    { "http://www.weather.com.cn/", "data/sk/101010100.html", AQRequestTypeWeather },
    { BASIC_URL, "/user/queryAllSurveyAn.htm", AQRequestTypeQueryAllQuestionsAndAnswers},
};
