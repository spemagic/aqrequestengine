//
//  AQRequestEngine+Request.m
//
//  Created by Zhanghao on 3/5/14.
//  Copyright (c) 2014 Zhanghao. All rights reserved.
//

#import "AQRequestEngine+Request.h"
#import <sys/sysctl.h>
#import "CommonCrypto/CommonDigest.h"
#import "AQRequestEngine.h"
#import "ASIFormDataRequest.h"
#import "DES3Util.h"

static NSString *REQ_TIME = nil;

@implementation AQRequestEngine (Request)

#pragma mark - Helper

- (NSString *)md5:(NSString *)string {
    const char *concat_str = [string UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(concat_str, strlen(concat_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < 16; i++) {
        [hash appendFormat:@"%02x", result[i]];
    }
    return hash;
}

- (void)setupBasicInfoForRequest:(ASIFormDataRequest *)request {
    //版本号
    NSString *app_version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [request setPostValue:app_version forKey:@"appVer"];
    
    //请求时间
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *reqTime = [formatter stringFromDate:[NSDate date]];
    [request setPostValue:reqTime forKey:@"reqTime"];
    REQ_TIME = reqTime;
#if !__has_feature(objc_arc)
    [formatter release];
#endif
    
    //设备类型
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [request setPostValue:@3 forKey:@"deviceType"];
    } else {
        [request setPostValue:@2 forKey:@"deviceType"];
    }
    
    //设备号
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"SYChronoID"]) {
        [request setPostValue:[defaults objectForKey:@"SYChronoID"] forKey:@"deviceCode"];
    } else {
        NSString *chronoId = [NSString stringWithFormat:@"%@%ld%@", reqTime, random(), app_version];
        NSString *md5ID = [self md5:chronoId];
        [defaults setValue:md5ID forKey:@"SYChronoID"];
        [request setPostValue:md5ID forKey:@"deviceCode"];
    }
}

#pragma mark - APIs

- (ASIHTTPRequest *)fetchTodayWeather {
    return [self getRequestWithType:AQRequestTypeWeather];
}

- (ASIFormDataRequest *)loginWithUserName:(NSString *)userName password:(NSString *)password {
    NSString *sign = [NSString stringWithFormat:@"%@%@%@", userName, REQ_TIME, CONVENSION_KEY];
    NSDictionary *parameters = @{
                                 @"loginName":userName,
                                 @"loginPwd":[DES3Util encrypt:password],
                                 @"sign":[self md5:sign]
                                 };
    return [self postRequestWithType:AQRequestTypeLogin value:parameters];
}

- (ASIFormDataRequest *)uploadFileWithFileName:(NSString *)fileName fileStream:(NSString *)fileStream {
    NSString *sign = [NSString stringWithFormat:@"%@%@%@", fileName, REQ_TIME, CONVENSION_KEY];
    NSDictionary *value = @{ @"fileName":fileName, @"sign":[self md5:sign] };
    
    NSDictionary *file = @{ @"fileStream" : fileStream };
    return [self postRequestWithType:AQRequestTypeUploadHeadImage value:value file:file];
}

- (ASIFormDataRequest *)queryAllQuestionsAndAnswersWithUserId:(NSString *)userId sessionId:(NSString *)sessionId {
    NSString *sign = [NSString stringWithFormat:@"%@%@", sessionId, CONVENSION_KEY];
    NSDictionary *value = @{ @"userId":userId, @"sessionId":sessionId, @"sign":[self md5:sign] };
    return [self postRequestWithType:AQRequestTypeQueryAllQuestionsAndAnswers value:value];
}

@end
