//
//  AQRequestEngine.m
//
//  Created by Zhanghao on 3/5/14.
//  Copyright (c) 2014 Zhanghao. All rights reserved.
//

#import "AQRequestEngine.h"
#import "AQRequestEngine+Request.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "AQError.h"

static NSUInteger REQUEST_COUNTER = 0;

static NSString *const requestIdKey = @"requestIdKey";
static NSString *const requestTypeKey = @"requestTypeKey";

@interface AQRequestEngine () <ASIHTTPRequestDelegate> {
    NSMutableArray *_requests;
}

@property (nonatomic, copy) AQRequestStartedBlock startedHandler;
@property (nonatomic, copy) AQRequestFinishedBlock finishedHandler;
@property (nonatomic, copy) AQRequestFailedBlock failedHandler;

@end

@implementation AQRequestEngine

#pragma mark - Lifecycle

- (id)init {
    self = [super init];
    if (self) {
        _requests = [[NSMutableArray alloc] init];
    }
    return self;
}

#if !__has_feature(objc_arc)
- (void)dealloc {
    [_requests release];
    _requests = nil;
    
    if (self.startedHandler) {
        [self.startedHandler release];
        self.startedHandler = nil;
    }
    if (self.finishedHandler) {
        [self.finishedHandler release];
        self.finishedHandler = nil;
    }
    if (self.failedHandler) {
        [self.failedHandler release];
        self.failedHandler = nil;
    }
    [super dealloc];
}
#endif

#pragma mark - Request Attributes

// 获取request在结构体中的下标
- (NSInteger)getRequestIndexAtRequestType:(AQRequestType)type {
    for (int i = 0; i < sizeof(reqeustDictionary) / sizeof(AQRequestDictionary); i++) {
        AQRequestDictionary dic = reqeustDictionary[i];
        if (dic.requestType == type) {
            return i;
        }
    }
    return INVALID_INDEX;
}

// 获取request在已发起的request列表中的下标
- (NSInteger)getRequestIdInRequestList:(ASIHTTPRequest *)request {
    if (!request.userInfo) {
        return INVALID_INDEX;
    }
    NSString *requestId = [request.userInfo valueForKey:requestIdKey];
    if (!requestId) {
        return INVALID_INDEX;
    }
    return [requestId integerValue];
}

// 获取该request的类型
- (AQRequestType)getRequestType:(ASIHTTPRequest *)request {
    if (!request.userInfo) {
        return AQRequestTypeInvalid;
    }
    NSString *requestType = [request.userInfo valueForKey:requestTypeKey];
    if (!requestType) {
        return AQRequestTypeInvalid;
    }
    return [requestType integerValue];
}

#pragma mark - URL

// 根据request类型自动组装完整的url地址
- (NSString *)getUrlWithRequestType:(AQRequestType)type {
    NSInteger requestIndex = [self getRequestIndexAtRequestType:type];
    const char *cBasicUrl = reqeustDictionary[requestIndex].basicUrl;
    const char *cAppendingUrl = reqeustDictionary[requestIndex].appendingUrl;
    NSString *basicUrl = [NSString stringWithCString:cBasicUrl encoding:NSUTF8StringEncoding];
    NSString *appendingUrl = [NSString stringWithCString:cAppendingUrl encoding:NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"%@%@", basicUrl, appendingUrl];
    return url;
}

#pragma mark - Request lifecycle

// 发起request
- (void)initiateRequest:(ASIHTTPRequest *)request {
    NSParameterAssert(request);
    [_requests addObject:request];
    [request setTimeOutSeconds:TIME_OUT];
    [request setDelegate:self];
    [request startAsynchronous];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

#pragma mark - Public methods

// 创建Post类型的request并且填充所需的post参数
- (ASIFormDataRequest *)postRequestWithType:(AQRequestType)type value:(NSDictionary *)value file:(NSDictionary *)file data:(NSDictionary *)data {
    NSString *urlString = [self getUrlWithRequestType:type];
    if (!urlString || ![urlString length]) {
        return nil;
    }
    NSString *escapedUrl = [urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:escapedUrl];
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [self setupBasicInfoForRequest:request];
    [request setRequestMethod:@"POST"];
    
    if (value) {
        for (NSString *key in value.allKeys) {
            [request setPostValue:value[key] forKey:key];
        }
    }
    
    if (file) {
        for (NSString *key in file.allKeys) {
            [request setFile:file[key] forKey:key];
        }
    }
    
    if (data) {
        for (NSString *key in data.allKeys) {
            [request setData:data[key] forKey:key];
        }
    }
    
    REQUEST_COUNTER++;
    
    NSString *requestId = [NSString stringWithFormat:@"%d", REQUEST_COUNTER];
    NSString *requestType = [NSString stringWithFormat:@"%d", type];
    request.userInfo = @{
                         requestIdKey:requestId,
                         requestTypeKey:requestType
                         };
    
    [self initiateRequest:request];
    return request;
}

- (ASIFormDataRequest *)postRequestWithType:(AQRequestType)type value:(NSDictionary *)value file:(NSDictionary *)file {
    return [self postRequestWithType:type value:value file:file data:nil];
}

- (ASIFormDataRequest *)postRequestWithType:(AQRequestType)type value:(NSDictionary *)value {
    return [self postRequestWithType:type value:value file:nil];
}

// 创建Get类型的request
- (ASIHTTPRequest *)getRequestWithType:(AQRequestType)type {
    NSString *urlString = [self getUrlWithRequestType:type];
    if (!urlString || ![urlString length]) {
        return nil;
    }
    NSString *escapedUrl = [urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:escapedUrl];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setRequestMethod:@"GET"];
    REQUEST_COUNTER++;
    
    NSString *requestId = [NSString stringWithFormat:@"%d", REQUEST_COUNTER];
    NSString *requestType = [NSString stringWithFormat:@"%d", type];
    request.userInfo = @{
                         requestIdKey:requestId,
                         requestTypeKey:requestType
                         };
    [self initiateRequest:request];
    return request;
}

// 取消指定的request
- (void)cancelRequest:(NSInteger)requestId {
    if (requestId == INVALID_INDEX) {
        return;
    }
    for (int i = 0; i < [_requests count]; i++) {
        ASIHTTPRequest *request = (ASIHTTPRequest *)_requests[i];
        NSInteger rId = [self getRequestIdInRequestList:request];
        if (rId == requestId) {
            [request cancel];
            return;
        }
    }
}

// 取消所有request
- (void)cancelAllRequests {
    for (int i = 0; i < [_requests count]; i++) {
        ASIHTTPRequest *request = (ASIHTTPRequest *)_requests[i];
        [request cancel];
    }
}

#pragma mark - Request block

- (void)setStartedBlock:(AQRequestStartedBlock)startedBlock {
    if (startedBlock) {
        self.startedHandler = startedBlock;
    }
}

- (void)setFinishedBlock:(AQRequestFinishedBlock)finishedBlock {
    if (finishedBlock) {
        self.finishedHandler = finishedBlock;
    }
}

- (void)setFailedBlock:(AQRequestFailedBlock)failedBlock {
    if (failedBlock) {
        self.failedHandler = failedBlock;
    }
}

#pragma mark - ASIHTTPRequestDelegate
- (void)requestStarted:(ASIHTTPRequest *)request {
    AQRequestType requestType = [self getRequestType:request];

    if (self.startedHandler) {
        self.startedHandler(requestType, request);
    }
    
    if ([self.delegate respondsToSelector:@selector(requestStarted:request:)]) {
        [self.delegate requestStarted:requestType request:request];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    NSParameterAssert(request);

    AQRequestType requestType = [self getRequestType:request];
    
    // 根据ASIHTTPRequest提供的错误码进行判断
    NSInteger code = [[request error] code];
	if (code > AQNetworkMaxError) {
        code = AQUnhandledExceptionError;
    }
	AQNetworkErrorSubcode subcode = (AQNetworkErrorSubcode)code;
    
    // 非状态码错误和数据解析错误都归结为网络错误，详细错误存放在subcode和description中
    AQError *error = [AQError errorWithCode:AQNetworkError
                                withSubcode:subcode
                            withDescription:REQUEST_FAILED
                            withDetailError:[request error]];
    
    if (self.failedHandler) {
        self.failedHandler(requestType, error);
    }
    
    if ([self.delegate respondsToSelector:@selector(requestFailed:error:)]) {
        [self.delegate requestFailed:requestType error:error];
    }
    
    if ([_requests containsObject:request]) {
        [_requests removeObject:request];
    }
    if ([_requests count] == 0) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    NSParameterAssert(request);

    AQRequestType requestType = [self getRequestType:request];
    if (requestType == AQRequestTypeInvalid) {
        // 未知API错误
        AQError *error = [AQError errorWithCode:AQAPIError
                                    withSubcode:AQAPIUnknownError
                                withDescription:[request responseStatusMessage]
                                withDetailError:[request error]];
        
        if (self.failedHandler) {
            self.failedHandler(requestType, error);
        }
        
        if ([self.delegate respondsToSelector:@selector(requestFailed:error:)]) {
            [self.delegate requestFailed:requestType error:error];
        }
    } else if (request.responseStatusCode != 200) {
        // response code非200错误
        AQError *error = [AQError errorWithCode:AQHttpError
                                    withSubcode:[request responseStatusCode]
                                withDescription:[request responseStatusMessage]
                                withDetailError:[request error]];
        if (self.failedHandler) {
            self.failedHandler(requestType, error);
        }
        
        if ([self.delegate respondsToSelector:@selector(requestFailed:error:)]) {
            [self.delegate requestFailed:requestType error:error];
        }
    } else {
        NSError *error = nil;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
        if (!error) {
            // 针对Chrono365的特殊字段result来判断request的成功和失败
            NSString *result = response[@"result"];
            if (result) {
                // request为1，成功
                if ([result isEqualToString:RESULT_SUCCESS]) {
                    if (self.finishedHandler) {
                        self.finishedHandler(requestType, response);
                    }
                    
                    if ([self.delegate respondsToSelector:@selector(requestFinished:response:)]) {
                        [self.delegate requestFinished:requestType response:response];
                    }
                } else {
                    // request为其他状态码，失败
                    AQError *error = [AQError errorWithCode:AQAPIError
                                                withSubcode:AQAPIParameterUnknownError
                                            withDescription:response[@"errorMsg"]
                                            withDetailError:[request error]];
                    if (self.failedHandler) {
                        self.failedHandler(requestType, error);
                    }
                    
                    if ([self.delegate respondsToSelector:@selector(requestFailed:error:)]) {
                        [self.delegate requestFailed:requestType error:error];
                    }
                }
            } else {
                
                // 如果没有result字段，暂时都认为request已经完成
                if (self.finishedHandler) {
                    self.finishedHandler(requestType, response);
                }
                
                if ([self.delegate respondsToSelector:@selector(requestFinished:response:)]) {
                    [self.delegate requestFinished:requestType response:response];
                }
            }
        } else {
            // JSON解析错误
            AQError *error = [AQError errorWithCode:AQJSONParseError
                                        withSubcode:AQAPIParameterUnknownError
                                    withDescription:PARAMETER_ERROR
                                    withDetailError:[request error]];
            if (self.failedHandler) {
                self.failedHandler(requestType, error);
            }
            
            if ([self.delegate respondsToSelector:@selector(requestFailed:error:)]) {
                [self.delegate requestFailed:requestType error:error];
            }
        }
    }
    
    if ([_requests containsObject:request]) {
        [_requests removeObject:request];
    }
    if ([_requests count] == 0) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

@end
