//
//  AQRequestEngine.h
//
//  Created by Zhanghao on 3/5/14.
//  Copyright (c) 2014 Zhanghao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AQRequestConfigure.h"

@class ASIHTTPRequest;
@class ASIFormDataRequest;
@class AQError;

@protocol AQRequestEngineDelegate;

typedef void (^AQRequestStartedBlock)(AQRequestType type, ASIHTTPRequest *request);
typedef void (^AQRequestFinishedBlock)(AQRequestType type, NSDictionary *response);
typedef void (^AQRequestFailedBlock)(AQRequestType type, AQError *error);

@interface AQRequestEngine : NSObject

#if __has_feature(objc_arc)
@property (nonatomic, weak) id<AQRequestEngineDelegate> delegate;
#else
@property (nonatomic, assign) id<AQRequestEngineDelegate> delegate;
#endif

- (ASIFormDataRequest *)postRequestWithType:(AQRequestType)type value:(NSDictionary *)value;
- (ASIFormDataRequest *)postRequestWithType:(AQRequestType)type value:(NSDictionary *)value file:(NSDictionary *)file;
- (ASIFormDataRequest *)postRequestWithType:(AQRequestType)type value:(NSDictionary *)value file:(NSDictionary *)file data:(NSDictionary *)data;

- (ASIHTTPRequest *)getRequestWithType:(AQRequestType)type;

- (void)initiateRequest:(ASIHTTPRequest *)request;
- (void)cancelRequest:(NSInteger)requestId;
- (void)cancelAllRequests;

- (void)setStartedBlock:(AQRequestStartedBlock)startedBlock;
- (void)setFinishedBlock:(AQRequestFinishedBlock)finishedBlock;
- (void)setFailedBlock:(AQRequestFailedBlock)failedBlock;

@end

@protocol AQRequestEngineDelegate <NSObject>

@optional
- (void)requestStarted:(AQRequestType)type request:(ASIHTTPRequest *)request;
- (void)requestFinished:(AQRequestType)type response:(NSDictionary *)response;
- (void)requestFailed:(AQRequestType)type error:(AQError *)error;

@end
