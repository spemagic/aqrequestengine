//
//  AQError.m
//
//  Created by Zhanghao on 3/5/14.
//  Copyright (c) 2014 Zhanghao. All rights reserved.
//

#import "AQError.h"

@implementation AQError

+ (AQError *)errorWithCode:(AQErrorCode)aCode
               withSubcode:(NSInteger)aSubcode
           withDescription:(NSString *)aDescription
           withDetailError:(NSError *)aDetail {
    AQError *error = [[AQError alloc] initWithCode:aCode
                                       withSubcode:aSubcode
                                   withDescription:aDescription
                                   withDetailError:aDetail];
#if !__has_feature(objc_arc)
    return [error autorelease];
#else
    return error;
#endif
}

- (AQError *)initWithCode:(AQErrorCode)aCode
              withSubcode:(NSInteger)aSubcode
          withDescription:(NSString *)aDescription
          withDetailError:(NSError *)aDetail {
    self = [super init];
    if (self) {
        self.code = aCode;
        self.subCode = aSubcode;
        self.description = aDescription;
        self.detail = aDetail;
    }
    return self;
}

- (NSString *)errorCodeStringAtCode:(AQErrorCode)code {
    if (code < AQHttpError) {
        return nil;
    }
    
    if (code > AQMaxError) {
        return nil;
    }
    
    const char *cCode = error_code_struct[code - 1].description;
    NSString *errorCodeString = [NSString stringWithCString:cCode encoding:NSUTF8StringEncoding];
    if (errorCodeString) {
        return errorCodeString;
    }
    
    return nil;
}

- (NSString *)errorSubCodeStringAtCode:(AQErrorCode)code subcode:(NSInteger)subcode {
    const char *cSubcode = NULL;
    NSString *subcodeString = nil;
    if (code > 0) {
        switch (code) {
            case AQHttpError:
            {
                subcodeString = [NSString stringWithFormat:@"HTTP状态码:%d", subcode];
                break;
            }
            case AQNetworkError:
            {
                if (subcode >= AQConnectionFailureError && subcode <= AQNetworkMaxError) {
                    cSubcode = network_error_subcode_struct[subcode - 1].description;
                    subcodeString = [NSString stringWithCString:cSubcode encoding:NSUTF8StringEncoding];
                }
                break;
            }
            case AQXMLParserError:
            {
                if (subcode >= AQXMLParseNoHeaderError && subcode <= AQXMLParseUnknownError) {
                    cSubcode = xml_parse_error_subcode_struct[subcode - 1].description;
                    subcodeString = [NSString stringWithCString:cSubcode encoding:NSUTF8StringEncoding];
                }
                break;
            }
            case AQJSONParseError:
            {
                if (subcode >= AQJSONParseNonStandardType && subcode <= AQJSONParseUnknownError) {
                    cSubcode = json_parse_error_subcode_struct[subcode - 1].description;
                    subcodeString = [NSString stringWithCString:cSubcode encoding:NSUTF8StringEncoding];
                }
                break;
            }
            case AQAPIError:
            {
                if (subcode >= AQAPIParameterUnknownError && subcode <= AQAPIMaxError) {
                    cSubcode = api_error_subcode_struct[subcode - 21000].description;
                    subcodeString = [NSString stringWithCString:cSubcode encoding:NSUTF8StringEncoding];
                }
                break;
            }
            default:
                break;
        }
    }
    return subcodeString;
}

//- (NSString *)description {
//    NSString *errorCode = [self errorCodeStringAtCode:self.code];
//    NSString *errorSubcode = [self errorSubCodeStringAtCode:self.code subcode:self.subCode];
//    return [NSString stringWithFormat:@"\n错误原因 : %@\n详细 : %@\n描述 : %@\nNSError : %@", errorCode, errorSubcode, _description, self.detail];
//}

#if !__has_feature(objc_arc)
- (void)dealloc {
    self.description = nil;
    self.detail = nil;
    [super dealloc];
}
#endif

@end
