//
//  AQRequestEngine+Request.h
//
//  Created by Zhanghao on 3/5/14.
//  Copyright (c) 2014 Zhanghao. All rights reserved.
//

#import "AQRequestEngine.h"

@interface AQRequestEngine (Request)

- (void)setupBasicInfoForRequest:(ASIFormDataRequest *)request;

// Get
- (ASIHTTPRequest *)fetchTodayWeather;

// Post
- (ASIFormDataRequest *)loginWithUserName:(NSString *)userName password:(NSString *)password;
- (ASIFormDataRequest *)uploadFileWithFileName:(NSString *)fileName fileStream:(NSString *)fileStream;
- (ASIFormDataRequest *)queryAllQuestionsAndAnswersWithUserId:(NSString *)userId sessionId:(NSString *)sessionId;

@end
