//
//  AQError.h
//
//  Created by Zhanghao on 3/5/14.
//  Copyright (c) 2014 Zhanghao. All rights reserved.
//

#import <Foundation/Foundation.h>

#define REQUEST_FAILED          @"请求失败"
#define PARAMETER_ERROR         @"服务端返回参数错误"

#define RESULT_SUCCESS          @"1"

typedef struct {
    unsigned int code;
    char *description;
} AQErrorCodeStruct;

typedef struct {
    unsigned int subcode;
    char *description;
} AQErrorSubcodeStruct;

// 错误主码
typedef enum {
    AQHttpError = 1,     // HTTP响应为非 200(ok) 错误, 详细请看subcode, subcode会含HTTP status错误代码，如404等
	AQNetworkError = 2,      // 网络错误, 详细请看subcode, subcode详情查看AQNetworkErrorCode
	AQXMLParserError = 3,    // XML解析错误, 详细请看subcode, subcode详情查看AQXMLParseErrorCode
    AQJSONParseError = 4,    // Json解析错误, 详细请看subcode, subcode详情查看AQJSONParseErrorCode
	AQAPIError = 5,          // 协议参数错误,服务器返回错误码, 详细请看subcode, subcode详情查看AQAPIErrorCode
    
	AQMaxError
} AQErrorCode;

static const AQErrorCodeStruct error_code_struct[] = {
    { AQHttpError, "HTTP状态码非200" },
    { AQNetworkError , "网络错误"},
    { AQXMLParserError, "XML解析错误" },
    { AQJSONParseError, "JSON解析错误" },
    { AQAPIError, "API参数错误" },
};


// ASIHTTPRequest网络错误子码
typedef enum {
    AQConnectionFailureError = 1,
    AQRequestTimedOutError = 2,
    AQAuthenticationError = 3,
    AQRequestCancelledError = 4,
    AQUnableToCreateRequestError = 5,
    AQInternalErrorWhileBuildingRequestError  = 6,
    AQInternalErrorWhileApplyingCredentialsError  = 7,
	AQFileManagementError = 8,
	AQTooMuchRedirectionError = 9,
	AQUnhandledExceptionError = 10,
	
	AQNetworkMaxError = AQUnhandledExceptionError
} AQNetworkErrorSubcode;

static const AQErrorSubcodeStruct network_error_subcode_struct[] = {
    { AQConnectionFailureError, "请求失败" },
    { AQRequestTimedOutError, "请求超时" },
    { AQAuthenticationError, "授权错误" },
    { AQRequestCancelledError, "请求取消" },
    { AQUnableToCreateRequestError, "无法创建请求" },
    { AQInternalErrorWhileBuildingRequestError, "创建请求内部错误" },
    { AQInternalErrorWhileApplyingCredentialsError, "内部认证错误" },
    { AQFileManagementError, "文件管理错误" },
    { AQTooMuchRedirectionError, "重定向次数过多" },
    { AQUnhandledExceptionError, "未经处理的异常错误" },
    { AQNetworkMaxError, "未知网络错误" },
};


// XML解析错误子码
typedef enum {
    AQXMLParseNoHeaderError = 1,
    AQXMLParselNoBodyError,
    AQXMLParseUnknownError,
} AQXMLParseErrorSubcode;

static const AQErrorCodeStruct xml_parse_error_subcode_struct[] = {
    { AQXMLParseNoHeaderError, "XML数据无数据头" },
    { AQXMLParselNoBodyError, "XML数据无主体" },
    { AQXMLParseUnknownError, "XML数据未知错误" },
};


// JSON解析错误子码
typedef enum {
    AQJSONParseNonStandardType = 1,
    AQJSONParseUnknownError
} AQJSONParseErrorCode;

static const AQErrorCodeStruct json_parse_error_subcode_struct[] = {
    { AQJSONParseNonStandardType, "非标准JSON数据" },
    { AQJSONParseUnknownError, "JSON数据未知错误" },
};


// API参数错误子码
typedef enum {
	// 10000以下的保留为HTTP错误代码使用，如501，404等
	
	// 10000 ~ 20000 内的为Reserved
	AQReservedBaseError = 10000,
	
	// 20000以上为服务器返回错误代码
	// 常规错误 20000
	AQAPIBaseError = 20000,
    
	// 参数错误:	1
	AQAPIParameterUnknownError = AQAPIBaseError + 1000,                // 1000	参数错误，未明确具体类别
	AQAPIParameterAbsenceError,                                        // 1001	缺少参数
	AQAPIParameterTypeIncorrectError,                                  // 1002	参数类型错误
	AQAPIParameterValueOverError,                                      // 1003	参数值错误，如超出取值范围等
    
    // 2 ~ 6 待扩展
    
	// 执行异常：7
	AQAPIExecuteExceptionError = AQAPIBaseError + 7000,             // 7000	程序执行时产生未处理的异常，程序出错
	AQAPIUnknownError,                                              // 未知错误（eg，未知API错误）
	AQAPIMaxError = AQAPIUnknownError
	
} AQAPIErrorCode;

static const AQErrorSubcodeStruct api_error_subcode_struct[] = {
	// 参数错误:	1
	{ AQAPIParameterUnknownError, "参数错误" },
	{ AQAPIParameterAbsenceError, "缺少参数" },
	{ AQAPIParameterTypeIncorrectError, "参数类型错误" },
	{ AQAPIParameterValueOverError, "参数值错误，如超出取值范围等" },
    
	// 扩展标识：2～6	待扩展
	
	// 执行异常：7
	{ AQAPIExecuteExceptionError, "程序执行时产生未处理的异常，程序出错" },
	{ AQAPIUnknownError, "未知错误" }
};


@interface AQError : NSObject

@property (nonatomic, assign) AQErrorCode code;
@property (nonatomic, assign) NSInteger subCode;
@property (nonatomic, copy) NSString *description;
#if !__has_feature(objc_arc)
@property (nonatomic, retain) NSError *detail;
#else
@property (nonatomic, strong) NSError *detail;
#endif

+ (AQError *)errorWithCode:(AQErrorCode)aCode
               withSubcode:(NSInteger)aSubcode
           withDescription:(NSString *)aDescription
           withDetailError:(NSError *)aDetail;

- (AQError *)initWithCode:(AQErrorCode)aCode
              withSubcode:(NSInteger)aSubcode
          withDescription:(NSString *)aDescription
          withDetailError:(NSError *)aDetail;

@end
