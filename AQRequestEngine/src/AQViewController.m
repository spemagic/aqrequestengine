//
//  AQViewController.m
//  AQRequestEngine
//
//  Created by Zhanghao on 3/5/14.
//  Copyright (c) 2014 Zhanghao. All rights reserved.
//

#import "AQViewController.h"
#import "AQRequest.h"

@interface AQViewController () <AQRequestEngineDelegate> {
    AQRequestEngine *engine;
}
@end

@implementation AQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
    engine = [[AQRequestEngine alloc] init];
    engine.delegate = self;
    
    [engine fetchTodayWeather];
//    [engine loginWithUserName:@"15618059591" password:@"111111"];
//    [engine uploadFileWithFileName:@"myHeadImage" fileStream:@"/Users/Zhanghao/Demo/checked.png"];
    
    __weak AQRequestEngine *wengine = engine;
    
    [engine setStartedBlock:^(AQRequestType type, ASIHTTPRequest *request) {
        
    }];
    
    [engine setFinishedBlock:^(AQRequestType type, NSDictionary *response) {
        if (type == AQRequestTypeLogin) {
            
            [wengine queryAllQuestionsAndAnswersWithUserId:response[@"userId"]
                                                 sessionId:response[@"sessionId"]];
            
        } else if (type == AQRequestTypeQueryAllQuestionsAndAnswers) {
            
        } else if (type == AQRequestTypeUploadHeadImage) {
            NSLog(@"%@", response);
        } else if (type == AQRequestTypeWeather) {
            NSLog(@"%@", response);
        }
    }];
    
    [engine setFailedBlock:^(AQRequestType type, AQError *error) {
        NSLog(@"error:%@", error);
    }];
}

@end
