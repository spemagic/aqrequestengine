//
//  main.m
//  AQRequestEngine
//
//  Created by Zhanghao on 3/5/14.
//  Copyright (c) 2014 Zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AQAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AQAppDelegate class]));
    }
}
